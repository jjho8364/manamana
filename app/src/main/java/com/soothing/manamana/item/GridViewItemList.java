package com.soothing.manamana.item;

public class GridViewItemList {
    private String title;
    private String update;
    private String imgUrl;
    private String listUrl;

    public GridViewItemList(String title, String update, String imgUrl, String listUrl) {
        this.title = title;
        this.update = update;
        this.imgUrl = imgUrl;
        this.listUrl = listUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }
}
