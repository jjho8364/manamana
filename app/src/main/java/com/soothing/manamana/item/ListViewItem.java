package com.soothing.manamana.item;

public class ListViewItem {
    String title;
    String viewUrl;

    public ListViewItem(String title, String viewUrl) {
        this.title = title;
        this.viewUrl = viewUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }
}
