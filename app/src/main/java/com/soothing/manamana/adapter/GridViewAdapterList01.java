package com.soothing.manamana.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.soothing.manamana.R;
import com.soothing.manamana.item.GridViewItemList01;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterList01 extends BaseAdapter {
    String TAG = "GridViewAdapterList01";
    Activity context;
    ArrayList<GridViewItemList01> gridArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<GridViewItemList01> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }

    public GridViewAdapterList01(){

    }

    public GridViewAdapterList01(Activity context, ArrayList<GridViewItemList01> gridArr, int layout) {
        this.context = context;
        this.gridArr = gridArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<GridViewItemList01>();
        arraylist.addAll(gridArr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.gridview_title);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        GridViewItemList01 data = gridArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());

        return rowView;
    }

    @Override
    public int getCount() {
        return gridArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
