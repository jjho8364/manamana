package com.soothing.manamana.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.soothing.manamana.R;
import com.soothing.manamana.item.ViewActivityItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewActivityAdapter extends BaseAdapter {
    String TAG = "ViewActivityAdapter";
    Activity context;
    ArrayList<ViewActivityItem> listArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<ViewActivityItem> arraylist;

    static class ViewHolder {
        public ImageView imageView;
    }

    public ViewActivityAdapter(){

    }

    public ViewActivityAdapter(Activity context, ArrayList<ViewActivityItem> listArr, int layout) {
        this.context = context;
        this.listArr = listArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<ViewActivityItem>();
        arraylist.addAll(listArr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.img_view);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        ViewActivityItem data = listArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }

        return rowView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
