package com.soothing.manamana.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.soothing.manamana.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class ListViewAdapter extends BaseAdapter {
    String TAG = "ListViewAdapter";
    Activity context;
    ArrayList<String> listArr;
    ArrayList<String> listPageUrlArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<String> arraylist;

    static class ViewHolder {
        public TextView title;
    }

    public ListViewAdapter(){

    }

    public ListViewAdapter(Activity context, ArrayList<String> listArr, ArrayList<String> listPageUrlArr, int layout) {
        this.context = context;
        this.listArr = listArr;
        this.listPageUrlArr = listPageUrlArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<String>();
        arraylist.addAll(listArr);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            GridViewAdapterList.ViewHolder viewHolder = new GridViewAdapterList.ViewHolder();
            viewHolder.title = (TextView)rowView.findViewById(R.id.item_tv_list);

            rowView.setTag(viewHolder);
        }

        GridViewAdapterList.ViewHolder holder = (GridViewAdapterList.ViewHolder)rowView.getTag();
        String data = listArr.get(position);
        String viewUrl = listPageUrlArr.get(position);
        SharedPreferences pref= this.context.getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String cekUrl = pref.getString(viewUrl, null);
        if(cekUrl != null && !cekUrl.equals("")){
            holder.title.setTextColor(context.getResources().getColor(R.color.purple_300));
        } else {
            holder.title.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.title.setText(data);

        return rowView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
