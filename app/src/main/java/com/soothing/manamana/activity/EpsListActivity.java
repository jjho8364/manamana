package com.soothing.manamana.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.soothing.manamana.R;
import com.soothing.manamana.adapter.ListViewAdapter;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class EpsListActivity extends Activity {
    private String TAG = " EpsListActivity - ";
    private ProgressDialog mProgressDialog;
    ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String firstUrl = "";
    private String mainImgUrl = "";
    private String mainTitle = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView storyView;

    private int adsCnt = 0;

    private String listUrl = "";

    // history
    String[] sfArr = new String[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_eps_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        firstUrl = intent.getStringExtra("firstUrl");
        mainImgUrl = intent.getStringExtra("imgUrl");
        mainTitle = intent.getStringExtra("title");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_eps);
        titleView = (TextView)findViewById(R.id.tv_list_eps);
        storyView = (TextView)findViewById(R.id.tv_list_story);
        listView = (ListView)findViewById(R.id.list_listview);

        if(mainImgUrl != null && !mainImgUrl.equals("")){
            Picasso.with(EpsListActivity.this).load(mainImgUrl).into(posterView);
        } else {
            posterView.setImageResource(R.drawable.no_image_png);
        }
        titleView.setText(mainTitle);

        getListView = new GetListView();
        getListView.execute();
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        ArrayList<String> listTitleArr = new ArrayList<String>();
        ArrayList<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(EpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                ////////////////// episode list //////////////
                Elements lists = doc.select(".listType li a");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).text();
                    String viewUrl = firstUrl + lists.get(i).attr("href");
                    listTitleArr.add(title);
                    listPageUrlArr.add(viewUrl);
                }

                //////////////// episode contents ////////////
                story = doc.select(".menuList li").text();

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            storyView.setText(story);

            //////////// set listview ///////////
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(EpsListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
            listView.setAdapter(new ListViewAdapter(EpsListActivity.this, listTitleArr, listPageUrlArr, R.layout.listviewitem_list));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(EpsListActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", listPageUrlArr.get(position));
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("title", listTitleArr.get(position));
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                }
            });

            /*for(int i=0 ; i<listTitleArr.size() ; i++){
                listView.g
            }*/

            mProgressDialog.dismiss();
        }
    }

}
