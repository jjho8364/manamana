package com.soothing.manamana.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.soothing.manamana.R;
import com.soothing.manamana.item.ViewActivityItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class ViewActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " ViewActivity - ";
    private ProgressDialog mProgressDialog;
    //private ListView listView;
    private WebView webView;
    private GetImageUrl getImageUrl = null;
    private String baseUrl = "";
    private String firstUrl = "";
    private String title = "";
    private ArrayList<ViewActivityItem> itemArr;

    private LinearLayout linearLayout;
    private TextView preEps;
    private TextView nextEps;
    private TextView preImg;
    private TextView nextImg;
    private String preUrl = "";
    private String nextUrl = "";

    AdView adView;
    SharedPreferences pref;

    private String html = "";

    // history
    String[] sfArr = new String[5];
    String nowTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view);

        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent mainIntent = getIntent();
        baseUrl = mainIntent.getStringExtra("listUrl");
        firstUrl = mainIntent.getStringExtra("firstUrl");
        title = mainIntent.getStringExtra("title");

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);

        linearLayout = (LinearLayout)findViewById(R.id.li_btns);
        preEps = (TextView)findViewById(R.id.tv_preeps);
        nextEps = (TextView)findViewById(R.id.tv_nexteps);
        preImg = (TextView)findViewById(R.id.preimg);
        nextImg = (TextView)findViewById(R.id.nextimg);

        preEps.setOnClickListener(this);
        nextEps.setOnClickListener(this);
        preImg.setOnClickListener(this);
        nextImg.setOnClickListener(this);

        linearLayout.setVisibility(View.GONE);
        preImg.setVisibility(View.VISIBLE);
        nextImg.setVisibility(View.VISIBLE);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //linearLayout.setVisibility(View.GONE);
            adView.destroy();
            adView.setVisibility(View.GONE);
            preImg.setVisibility(View.VISIBLE);
            nextImg.setVisibility(View.VISIBLE);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //linearLayout.setVisibility(View.VISIBLE);
            //preImg.setVisibility(View.GONE);
            //nextImg.setVisibility(View.GONE);
        }

        getSfPref();
        if(baseUrl != null && !baseUrl.equals("")){
            pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString(baseUrl, "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.commit(); //완료한다.
        }

        getImageUrl = new GetImageUrl();
        getImageUrl.execute();

    }

    public class GetImageUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            itemArr = new ArrayList<ViewActivityItem>();
            html = "";

            mProgressDialog = new ProgressDialog(ViewActivity.this);
            mProgressDialog.setTitle("이미지를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document btnDoc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();
                btnDoc = Jsoup.connect(baseUrl).timeout(15000).get();

                Element iframe = doc.select("#iframe").first();
                String iframeSrc = iframe.attr("src");

                if(iframeSrc != null) {
                    doc = Jsoup.connect(iframeSrc).timeout(20000).get();
                }

                Elements lists = doc.select(".listType.view li img");

                html += "<html>";
                html += "   <head><style>img{display: inline;height: auto;max-width: 100%;}</style></head>";
                html += "   <body>";
                html += "       <div>";

                for(int i=0 ; i<lists.size() ; i++) {
                    String imgUrl = lists.get(i).attr("src");
                    itemArr.add(new ViewActivityItem(imgUrl));

                    html += "           <img src='" + imgUrl + "'>";


                    if(i != lists.size()-1) {
                        html += "        <br /><hr><br />";
                    }

                }

                html += "       <div>";
                html += "   </body>";
                html += "</html>";

                // preeps and next eps
                Elements btns = btnDoc.select(".btnC a");

                for(int i=0 ; i<btns.size() ; i++) {
                    String epsUrl = firstUrl + btns.get(i).attr("href");
                    String text = btns.get(i).text();

                    if(text.equals("이전 보기")) preUrl = epsUrl;
                    if(text.equals("다음 보기")) nextUrl = epsUrl;
                }

                // history
                Elements navs = btnDoc.select("#nav ul li");
                nowTitle = navs.get(navs.size()-1).text();

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pushSfArr();
            sfArr[0] = nowTitle + "`" + baseUrl;
            setSfArr();

            if(ViewActivity.this != null){
                webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //int orientation = this.getResources().getConfiguration().orientation;

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //linearLayout.setVisibility(View.VISIBLE);
            //preImg.setVisibility(View.GONE);
           // nextImg.setVisibility(View.GONE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //linearLayout.setVisibility(View.GONE);
            adView.destroy();
            adView.setVisibility(View.GONE);
            preImg.setVisibility(View.VISIBLE);
            nextImg.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_preeps :
                if(preUrl !=null && !preUrl.equals("")) {
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", preUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "이전화가 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_nexteps :
                if(nextUrl !=null && !nextUrl.equals("")) {
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", nextUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "다음화가 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.preimg :
                if(preUrl !=null && !preUrl.equals("")) {
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", preUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "이전화가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.nextimg :
                if(nextUrl !=null && !nextUrl.equals("")) {
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", nextUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "다음화가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void pushSfArr(){
        sfArr[4] = sfArr[3];
        sfArr[3] = sfArr[2];
        sfArr[2] = sfArr[1];
        sfArr[1] = sfArr[0];
    }

    public void setSfArr(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("one", sfArr[0]);
        editor.putString("two", sfArr[1]);
        editor.putString("three", sfArr[2]);
        editor.putString("four", sfArr[3]);
        editor.putString("five", sfArr[4]);
        editor.commit();

    }

    public void getSfPref(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        sfArr[0] = sf.getString("one", "");
        sfArr[1] = sf.getString("two", "");
        sfArr[2] = sf.getString("three", "");
        sfArr[3] = sf.getString("four", "");
        sfArr[4] = sf.getString("five", "");
    }

}
